//
//  ViewController.m
//  lessonTwo
//
//  Created by hh on 16/4/20.
//  Copyright © 2016年 hh. All rights reserved.
//

#import "ViewController.h"
#import "Person.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>
{
    NSArray *data;
}
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    // 枚举
    [tableView setDataSource:self];
    tableView.delegate = self;
    [self.view addSubview:tableView];
    
    NSBundle *bundle = [NSBundle mainBundle];
    NSURL *url = [bundle URLForResource:@"00002" withExtension:@"vcf"];
    
    NSString *text = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
    NSLog(@"读取数据：%@", text);
    
    [self parseVCardString:text];
}

-(void)parseVCardString:(NSString*)vcardString
{
    NSArray *lines = [vcardString componentsSeparatedByString:@"\n"];
    
    NSMutableArray<Person *> *d = [NSMutableArray new];
    Person *item;
    for(NSString* line in lines)
    {
        if ([line hasPrefix:@"BEGIN"])
        {
            // NSlog(@"parse start");
            item = [Person new];
        }else if ([line hasPrefix:@"END"])
        {
            // NSlog(@"parse end");
            [d addObject:item];
        }else if ([line hasPrefix:@"N;"])
        {
            // N;CHARSET=UTF-8;ENCODING=QUOTED-PRINTABLE:=E4=BD=99;=E9=86=B4;;;
            NSArray *upperComponents = [line componentsSeparatedByString:@":"];
            NSArray *components = [[upperComponents objectAtIndex:1] componentsSeparatedByString:@";"];
            
            NSString * lastName = [components objectAtIndex:0];
            NSString * firstName = [components objectAtIndex:1];
            
            lastName = [lastName stringByReplacingOccurrencesOfString:@"=" withString:@"%"];
            firstName = [firstName stringByReplacingOccurrencesOfString:@"=" withString:@"%"];
            
            // NSlog(@"name %@%@",[lastName stringByRemovingPercentEncoding],[firstName stringByRemovingPercentEncoding]);
        }else if ([line hasPrefix:@"EMAIL;"])
        {
            NSArray *components = [line componentsSeparatedByString:@":"];
            NSString *emailAddress = [components objectAtIndex:1];
            // NSlog(@"emailAddress %@",emailAddress);
            
        }else if ([line hasPrefix:@"TEL;"])
        {
            NSArray *components = [line componentsSeparatedByString:@":"];
            NSString *phoneNumber = [components objectAtIndex:1];
            // NSlog(@"phoneNumber %@",phoneNumber);
            item.tel = phoneNumber;
        }else if ([line hasPrefix:@"FN;"]){
            //FN;CHARSET=UTF-8;ENCODING=QUOTED-PRINTABLE:=E4=BD=99=E9=86=B4
            NSArray *upperComponents = [line componentsSeparatedByString:@":"];
            NSString *name = upperComponents[1];
            name = [name stringByReplacingOccurrencesOfString:@"=" withString:@"%"];
            name = [name stringByRemovingPercentEncoding];
            // NSlog(@"FN:%@",name);
            item.name = name;
        }else if ([line hasPrefix:@"ORG;"]){
            NSArray *upperComponents = [line componentsSeparatedByString:@":"];
            NSString *name = upperComponents[1];
            name = [name stringByReplacingOccurrencesOfString:@"=" withString:@"%"];
            name = [name stringByRemovingPercentEncoding];
            // NSlog(@"ORG:%@",name);
        }
    }
    data = d;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return data.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:nil];
    int row = [indexPath row];
    Person *p = [data objectAtIndex:row];
    cell.textLabel.text = p.name;
    cell.detailTextLabel.text = [p.tel copy];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Person *p = [data objectAtIndex:indexPath.row];
    NSString *tel = [NSString stringWithFormat:@"tel://%@",p.tel];
    NSURL *url = [NSURL URLWithString:tel];
    UIApplication *app = [UIApplication sharedApplication];
    [app openURL:url];
    NSLog(@"%@",indexPath);
}

//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
