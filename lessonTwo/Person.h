//
//  Person.h
//  lessonTwo
//
//  Created by hh on 16/4/25.
//  Copyright © 2016年 hh. All rights reserved.
//  GNU step  nil

#import <UIKit/UIKit.h>

@interface Person : NSObject
{
    int m;
}
 // strong weak unsafe_unretained assign copy
@property (strong,nonatomic) NSString *name;
@property (strong,nonatomic) NSString *tel;

// getter,  setter 访问器（读取／写入）

- (void)call;

@end
